package de.szut.store.mapping;

import de.szut.store.article.AddArticleDto;
import de.szut.store.supplier.AddSupplierDto;
import de.szut.store.article.GetAllArticlesBySupplierIdDto;
import de.szut.store.article.GetArticleDto;
import de.szut.store.article.ArticleEntity;
import de.szut.store.contact.ContactEntity;
import de.szut.store.supplier.SupplierEntity;
import de.szut.store.supplier.GetSupplierDto;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class MappingService {
    public SupplierEntity mapAddSupplierDtoToSupplier(AddSupplierDto dto){
        SupplierEntity newSupplier = new SupplierEntity();
        newSupplier.setName(dto.getName());
        ContactEntity newContact = new ContactEntity();
        newContact.setStreet(dto.getStreet());
        newContact.setPostcode(dto.getPostcode());
        newContact.setCity(dto.getCity());
        newContact.setPhone(dto.getPhone());
        newSupplier.setContact(newContact);
        newContact.setSupplier(newSupplier);
        return newSupplier;
    }

    public GetSupplierDto mapSupplierToGetSupplierDto(SupplierEntity supplier){
        GetSupplierDto dto = new GetSupplierDto();
        dto.setSid(supplier.getSid());
        dto.setName(supplier.getName());
        dto.setStreet(supplier.getContact().getStreet());
        dto.setPostcode(supplier.getContact().getPostcode());
        dto.setCity(supplier.getContact().getCity());
        dto.setPhone(supplier.getContact().getPhone());
        return dto;
    }

    public GetAllArticlesBySupplierIdDto mapSupplierToGetAllArticlesBySupplierIdDto(SupplierEntity supplier){
        GetAllArticlesBySupplierIdDto dto = new GetAllArticlesBySupplierIdDto();
        dto.setSupplierId(supplier.getSid());
        dto.setName(supplier.getName());
        Set<GetArticleDto> allArticles = new HashSet<>();
        for (ArticleEntity article: supplier.getArticles()) {
            GetArticleDto aDto = new GetArticleDto();
            aDto.setAid(article.getAid());
            aDto.setDesignation(article.getDesignation());
            aDto.setPrice(article.getPrice());
            allArticles.add(aDto);
        }
        dto.setAllArticles(allArticles);
        return dto;
    }

    public ArticleEntity mapAddArticleDtoToArticle(AddArticleDto dto){
        ArticleEntity entity = new ArticleEntity();
        entity.setDesignation(dto.getDesignation());
        entity.setPrice(dto.getPrice());
        return entity;
    }

    public GetArticleDto mapArticleToGetArticleDto(ArticleEntity entity){
        GetArticleDto dto = new GetArticleDto();
        dto.setAid(entity.getAid());
        dto.setDesignation(entity.getDesignation());
        dto.setPrice(entity.getPrice());
        return dto;
    }
}
