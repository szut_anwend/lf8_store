package de.szut.store.supplier;

import de.szut.store.article.ArticleEntity;
import de.szut.store.contact.ContactEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name="supplier")
public class SupplierEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sid;

    private String name;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private ContactEntity contact;

    @OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<ArticleEntity> articles;

    public void addArticle(ArticleEntity article){
        articles.add(article);
    }
}
