package de.szut.store.supplier;

import de.szut.store.exceptionhandling.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SupplierService {
    private final SupplierRepository repository;

    public SupplierService(SupplierRepository repository) {
        this.repository = repository;
    }

    public SupplierEntity create(SupplierEntity newSupplier) {
        return repository.save(newSupplier);
    }

    public List<SupplierEntity> readAll() {
        List<SupplierEntity> listSupplier = repository.findAll();
        return listSupplier;
    }

    public SupplierEntity readById(long id) {
        Optional<SupplierEntity> oSupplier = repository.findById(id);
        if (oSupplier.isEmpty()) {
           throw new ResourceNotFoundException("Supplier not found on id = " + id);
        }
        return oSupplier.get();
    }

    public SupplierEntity update(SupplierEntity supplier) {
        SupplierEntity updatedSupplier = readById(supplier.getSid());
        updatedSupplier.setName(supplier.getName());
        updatedSupplier.getContact().setStreet(supplier.getContact().getStreet());
        updatedSupplier.getContact().setPostcode(supplier.getContact().getPostcode());
        updatedSupplier.getContact().setCity(supplier.getContact().getCity());
        updatedSupplier.getContact().setPhone(supplier.getContact().getPhone());
        updatedSupplier = this.repository.save(updatedSupplier);
        return updatedSupplier;
    }

    public void delete(long id) {
        SupplierEntity toDelete = readById(id);
        repository.deleteById(id);
    }
}

