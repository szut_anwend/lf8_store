package de.szut.store.supplier;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class GetSupplierDto {
    private Long sid;
    private String name;
    private String street;
    private String postcode;
    private String city;
    private String phone;
}
