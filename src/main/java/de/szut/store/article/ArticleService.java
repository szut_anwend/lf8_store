package de.szut.store.article;

import de.szut.store.exceptionhandling.ResourceNotFoundException;
import de.szut.store.supplier.SupplierService;
import de.szut.store.supplier.SupplierEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ArticleService {
    private final ArticleRepository repository;
    private final SupplierService supplierService;

    public ArticleService(ArticleRepository repository, SupplierService supplierService) {
        this.repository = repository;
        this.supplierService = supplierService;
    }

    public List<ArticleEntity> readAll(){
        return this.repository.findAll();
    }

    public ArticleEntity readById(long id){
        ArticleEntity article = this.repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Article not found on : "+ id));
        return article;
    }

    public ArticleEntity readByDesignation(String designation){
        ArticleEntity article = this.repository.findByDesignation(designation);
        if(article == null){
            new ResourceNotFoundException("Article not found on : "+ designation);
        }
        return article;
    }

    public ArticleEntity createBySupplierId(long supplierId, ArticleEntity article) {
        SupplierEntity supplier = this.supplierService.readById(supplierId);
        article.setSupplier(supplier);
        supplier.addArticle(article);
        return this.repository.save(article);
    }
}
