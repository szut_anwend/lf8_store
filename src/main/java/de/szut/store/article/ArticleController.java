package de.szut.store.article;

import de.szut.store.mapping.MappingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/store/article")
public class ArticleController {
    private final ArticleService service;
    private final MappingService mappingService;

    public ArticleController(ArticleService service, MappingService mappingService) {
        this.service = service;
        this.mappingService = mappingService;
    }

    @GetMapping()
    public ResponseEntity<List<GetArticleDto>> getAllArticles(){
        List<ArticleEntity> articles = this.service.readAll();
        List<GetArticleDto> list = new LinkedList<>();
        for (ArticleEntity article: articles) {
            list.add(this.mappingService.mapArticleToGetArticleDto(article));
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetArticleDto> getArticleById(@PathVariable Long id) {
        ArticleEntity article = this.service.readById(id);
        GetArticleDto dto = this.mappingService.mapArticleToGetArticleDto(article);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("/{designation}")
    public ResponseEntity<GetArticleDto> getArticleByDesignation(@PathVariable final String designation){
        ArticleEntity article = this.service.readByDesignation(designation);
        GetArticleDto dto = this.mappingService.mapArticleToGetArticleDto(article);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("/{supplierId}")
    public ResponseEntity<GetArticleDto> createArticle(@PathVariable Long supplierId, @Valid @RequestBody final AddArticleDto dto){
        ArticleEntity article= this.mappingService.mapAddArticleDtoToArticle(dto);
        article = this.service.createBySupplierId(supplierId, article);
        GetArticleDto request = this.mappingService.mapArticleToGetArticleDto(article);
        return new ResponseEntity<>(request, HttpStatus.CREATED);
    }
}