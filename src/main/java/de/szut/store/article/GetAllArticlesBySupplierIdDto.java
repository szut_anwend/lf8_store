package de.szut.store.article;

import lombok.Data;

import java.util.Set;

@Data
public class GetAllArticlesBySupplierIdDto {
    private long supplierId;
    private String name;
    private Set<GetArticleDto> allArticles;
}
