package de.szut.store.article;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AddArticleDto {
    @NotBlank(message = "Designation is mandatory")
    private String designation;

    @NotNull
    private Double price;
}
