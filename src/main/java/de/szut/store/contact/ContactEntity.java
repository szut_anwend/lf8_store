package de.szut.store.contact;

import de.szut.store.supplier.SupplierEntity;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name="supplier_contact")
public class ContactEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cid;

    private String street;

    @Column(name = "zip")
    private String postcode;

    private String city;

    private String phone;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL,
            mappedBy = "contact")
    private SupplierEntity supplier;
}
